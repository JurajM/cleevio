package com.example.watcheshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.watcheshop.entity.Watch;
import com.example.watcheshop.service.WatchService;

@RestController
public class WatchController {
	
	@Autowired
	WatchService watchService;
	
	
	@PostMapping("/watches")
	@ResponseBody
	public ResponseEntity<Object> setWatch(@RequestBody Watch watch) {
		return new ResponseEntity<>(watchService.createWatch(watch), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/watches/{id}")
	@ResponseBody
	public ResponseEntity<Object> deleteWatch(@PathVariable Long id){
		
		watchService.deleteWatch(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/watches")
	@ResponseBody
	public ResponseEntity<Object> all() {
		return new ResponseEntity<>(watchService.all(), HttpStatus.OK);
	}
	
	@GetMapping("/watches/{id}")
	@ResponseBody
	public ResponseEntity<Object> getWatch(@PathVariable Long id){
		return new ResponseEntity<>(watchService.getWatch(id),HttpStatus.OK);
	}
	
	@PutMapping("/watches/{id}")
	@ResponseBody
	public ResponseEntity<Object> updateWatch(@PathVariable Long id, @RequestBody Watch watch){
		return new ResponseEntity<>(watchService.updateWatch(id, watch), HttpStatus.OK);
	}
	
	@GetMapping("/watches/page")
	@ResponseBody
	public ResponseEntity<Object> getPage(
			@RequestParam(name = "page") int page, 
			@RequestParam(name = "size") int size,
			@RequestParam(name = "sort") String sortBy,
			@RequestParam(name = "sortAsc") boolean sortDirection){
		return new ResponseEntity<>(watchService.getPage(page, size, sortBy, sortDirection), HttpStatus.OK);
	}
	
}
