package com.example.watcheshop.service;


import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.watcheshop.repository.WatchRepository;
import com.example.watcheshop.entity.Watch;


@Service
public class WatchService {
	
	@Autowired
	WatchRepository watchRepository;
	   
	/**
	 * Method for  storing watch in database
	 * @param watch represents data from request body
	 */
	public Watch createWatch(Watch watch) {
		return watchRepository.save(watch);
	}
	
	/**
	 * Method for getting all watches from database
	 * @return
	 */
	public List<Watch> all(){
		return watchRepository.findAll();
	}
	
	/**
	 * 
	 * @param id of Watch
	 * @return watch 
	 * @throw EntityNotFoundException is case the Watch you are trying to get does not exists
	 */
	public Watch getWatch(Long id) {
		var val = watchRepository.findById(id);
		if(val.isPresent()) {
			return val.get();
		}
		else {
			throw new EntityNotFoundException("Watch you are trying to get does not exist");
		}
	}
	
	/**
	 * 
	 * @param id of Watch which will be updated
	 * @param newWatch represents the new data
	 * @return updated watch
	 * @throws EntityNotFoundException is case the Watch you are trying to update does not exists
	 */
	public Watch updateWatch(Long id, Watch newWatch) {
		var val =  watchRepository.findById(id);
		if(val.isPresent()) {
			Watch watch = val.get();
			watch.setTitle(newWatch.getTitle());
			watch.setDescription(newWatch.getDescription());
			watch.setPrice(newWatch.getPrice());
			watch.setFountain(newWatch.getFountain());
			watchRepository.save(watch);
			return watch;	
		}else {
			throw new EntityNotFoundException("Watch you are trying to update does not exist");
		}
		
	}
	
	/**
	 * 
	 * @param id of Watch, which will be deleted
	 * @throws EmptyResultDataAccessException is case the Watch you are trying to delete does not exists
	 */
	public void deleteWatch(Long id) {
		 try{
			 watchRepository.deleteById(id);
		 }catch(EmptyResultDataAccessException ex) {
			 throw new EmptyResultDataAccessException("Watch with selected id does not exist", 1);
		 }
	}
	
	/**
	 * 
	 * @param page specify the Page you want to get on return
	 * @param size of Page
	 * @param sortBy specify parameter, you want to sort pages
	 * @param sortDirection specify, if sorting is ascending or descending
	 * @return
	 */
	public Page<Watch> getPage(int page, int size, String sortBy, boolean sortDirection){
		
		Pageable firstPageWithTwoElements;
		if(sortDirection) {
			firstPageWithTwoElements = PageRequest.of(page, size, Sort.by(sortBy).ascending());
		}else {
			firstPageWithTwoElements = PageRequest.of(page, size, Sort.by(sortBy).descending());
		}
		return watchRepository.findAll(firstPageWithTwoElements);
	}
}
