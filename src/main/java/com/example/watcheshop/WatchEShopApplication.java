package com.example.watcheshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WatchEShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(WatchEShopApplication.class, args);
	}

}
