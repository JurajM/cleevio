package com.example.watcheshop.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.ToString;
import lombok.Setter;
import lombok.Getter;

/**
 * Represents an Watch Entity inside Database.
 * This class is being used for request body data serialization 
 */

@SuppressWarnings("serial")
@XmlRootElement
@Entity
@Setter
@Getter
@ToString
public class Watch implements Serializable{
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "Title is mandatory")
	private  String title;
	
	@NotNull(message = "Price is mandatory")
	private  int price;
	
	@NotBlank(message = "Description is mandatory")
	private  String description;
	
	@NotBlank(message = "Fontain is mandatory")
	private String fountain;
	
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setFountain(String fountain) {
		this.fountain = fountain;
	}
	
	
	public Long getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public String getDescription() {
		return this.description;
	}

	
	public String getFountain() {
		return this.fountain;
	}
}
