package com.example.watcheshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.watcheshop.entity.Watch;

@Repository
public interface WatchRepository extends JpaRepository<Watch, Long>{

}
