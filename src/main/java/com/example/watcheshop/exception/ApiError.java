package com.example.watcheshop.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

class ApiError {

	   private HttpStatus status;
	   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	   private LocalDateTime timestamp;
	   private String message;

	   private ApiError() {
	       timestamp = LocalDateTime.now();
	   }

	   ApiError(HttpStatus status) {
	       this();
	       this.status = status;
	   }
	   
	   public HttpStatus getStatus() {
		   return this.status;
	   }
	   
	   public String getMessage() {
		   return this.message;
	   }
	   
	   public void setMessage(String message) {
		   this.message = message;
	   }
	}
