package com.example.watcheshop.exception;

import javax.persistence.EntityNotFoundException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
  
  
	/**
	 * Handle EntityNotFoundException
	 * @param ex EntityNotFoundException
	 * @return the ApiError object
	 */
	@ExceptionHandler(EntityNotFoundException.class)
   protected ResponseEntity<Object> handleEntityNotFound(
           EntityNotFoundException ex) {
       ApiError apiError = new ApiError(HttpStatus.NOT_FOUND);
       apiError.setMessage(ex.getMessage());
       return buildResponseEntity(apiError);
   }
   
	/**
	 * Handle IllegalArgumentException
	 * @param ex IllegalArgumentException
	 * @return the ApiError object
	 */
   @ExceptionHandler(IllegalArgumentException .class)
   protected ResponseEntity<Object> handleIllegalArgumen(
		   IllegalArgumentException ex){
	   ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
       apiError.setMessage(ex.getMessage());
       return buildResponseEntity(apiError);
   }
   
  /**
   * Handle EmptyResultDataAccessException
   * @param ex EmptyResultDataAccessException
   * @return the ApiError object
   */
   @ExceptionHandler(EmptyResultDataAccessException.class)
   protected ResponseEntity<Object> handleEmptyResultData(
		   EmptyResultDataAccessException ex){
	   ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
       apiError.setMessage(ex.getMessage());
       return buildResponseEntity(apiError);
   }
   
   private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
       return new ResponseEntity<>(apiError, apiError.getStatus());
   }
   
}
